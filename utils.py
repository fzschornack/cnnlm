# tensorflow
import tensorflow as tf
import numpy as np

# nlp processing
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
import nltk.data
import os
import re
import csv
import codecs
import pandas as pd

# word2vec
from gensim.models import KeyedVectors

def weight_variable(shape):
    '''Helper function to create a weight variable initialized with
    a normal distribution
    Parameters
    ----------
    shape : list
        Size of weight variable
    '''
    initial = tf.random_normal(shape, mean=0.0, stddev=0.01)
    return tf.Variable(initial)

def bias_variable(shape):
    '''Helper function to create a bias variable initialized with
    a constant value.
    Parameters
    ----------
    shape : list
        Size of weight variable
    '''
    initial = tf.random_normal(shape, mean=0.0, stddev=0.01)
    return tf.Variable(initial)

# The function "text_to_wordlist" is from
# https://www.kaggle.com/currie32/quora-question-pairs/the-importance-of-cleaning-text
def text_cleaner(text, remove_stopwords=False, stem_words=False):
    # Clean the text, with the option to remove stopwords and to stem words.
    
    # Convert words to lower case and split them
    text = text.lower().split()

    # Optionally, remove stop words
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        text = [w for w in text if not w in stops]
    
    text = " ".join(text)

    # Clean the text
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "cannot ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " to the power of ", text)
    text = re.sub(r"\+", " plus ", text)
    text = re.sub(r"\-", " minus ", text)
    text = re.sub(r"\=", " equals ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", "", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)
    
    # Optionally, shorten words to their stems
    if stem_words:
        text = text.split()
        stemmer = SnowballStemmer('english')
        stemmed_words = [stemmer.stem(word) for word in text]
        text = " ".join(stemmed_words)
    
    return text.rstrip().lstrip()
    # Return a list of words
    # return text.rstrip().split(" ")

def read_train_file(TRAIN_DATA_FILE):
    sentences = [] 
    with codecs.open(TRAIN_DATA_FILE, encoding='utf-8') as f:
        lines = f.readlines()
        for line in lines:
            sentences.append(line)
    print('Found %s texts in sentences_file.txt' % len(sentences))

    return sentences

# Transform sentence (wordlist) in a matrix of word2vec embeddings
def prepare_embedding(wordlist, word2vec, embedding_dim):
    nb_words = len(wordlist)

    embedding_matrix = np.zeros((nb_words, embedding_dim))
    for i in range(len(wordlist)):
        if wordlist[i] in word2vec.vocab:
            embedding_matrix[i] = word2vec.word_vec(wordlist[i])
    # print('Null word embeddings: %d' % np.sum(np.sum(embedding_matrix, axis=1) == 0))
    
    return np.array(embedding_matrix)

# Prepare input data. Each sentence generates many training examples
# Ex.: [i, Like, dark,chocolate] generates
# x1 = [i, Like, dark,chocolate], x2 = [i],             y = [like]
# x1 = [i, Like, dark,chocolate], x2 = [i, like],       y = [dark]
# x1 = [i, Like, dark,chocolate], x2 = [i, like, dark], y = [chocolate]
def prepare_input(embedded_matrix, wordlist):
    sentence_list = list()
    previous_words_list = list()
    next_word_list = list()
    whole_sentences = list()
    for i in range(1, embedded_matrix.shape[0]):
        word_found = np.sum(embedded_matrix[i], axis=0) != 0
        if word_found: 
            sentence_list.append(embedded_matrix)
            previous_words_list.append(embedded_matrix[:i])
            next_word_list.append(wordlist[i])
            whole_sentences.append(" ".join(wordlist))
        
    return sentence_list, previous_words_list, next_word_list, whole_sentences