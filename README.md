# Configurando o ambiente

1 - Instalar anaconda (64 bits)

2 - No terminal executar:

```
conda create --name tensorflow python=3.5
activate tensorflow
conda install keras
conda install scipy
conda install nltk
conda install gensim
pip install tensorflow
```

*Utilizar o 'pip' caso o pacote não seja encontrado pelo 'conda'*

3 - Fazer o download dos principais módulos do nltk:

```
$ python
import nltk
nltk.download()
```

4 - Fazer o download do modelo pre-treinado do Google news (word2vec) e extraí-lo na raiz do projeto:
https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit

5 - criar a pasta "model" na raiz do projeto e colocar dentro dela o último modelo treinado.

# Treinando o modelo

```
python cnnlm_model.py
```

# Utilizando o modelo (exemplo)

```
python cnnlm_classifier_quora.py 
```