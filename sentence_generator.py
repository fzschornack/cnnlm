import codecs
import nltk.data
import re
import os

from utils import text_cleaner

text = []
with codecs.open("data/wiki_08", encoding='utf-8') as f:
    data = f.readlines()
    for line in data:
        text.append(line)
print('Found %s texts' % len(text))

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

with open("data/sentences_file_10.txt", "a") as output:
    for line in text:
        line = re.sub(r";", " . ", line)
        sentences = tokenizer.tokenize(line)
        if len(sentences) < 2: continue
        for s in sentences:
            s_cleaned = text_cleaner(s, remove_stopwords=False)
            if len(s_cleaned.split(" ")) >= 10:
                output.write(s_cleaned + "\n")