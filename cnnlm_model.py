
# coding: utf-8

# In[1]:

# tensorflow
import tensorflow as tf
import numpy as np

# word2vec
from gensim.models import KeyedVectors

# utils
from utils import prepare_embedding, prepare_input, weight_variable, bias_variable, read_train_file
from keras.preprocessing.text import Tokenizer
import math
from time import gmtime, strftime
from operator import itemgetter
import random
import os

import keras.preprocessing.text # python 2.7 keras bug - https://github.com/fchollet/keras/issues/1072

# In[2]:

# Parameters
TRAIN_DATA_FILE = 'data/sentences_file.txt'
EMBEDDING_DIM = 300
MAX_NB_WORDS = 200000
EMBEDDING_FILE = '/datasets/nlp/wordvecs/word2vec/GoogleNews-vectors-negative300.bin'
MODEL_PATH = './model'
if not os.path.exists(MODEL_PATH):
    os.makedirs(MODEL_PATH)

# load pre-trained word2vec model (take a while...)
word2vec = KeyedVectors.load_word2vec_format(EMBEDDING_FILE,         binary=True)


# python 2.7 keras bug - https://github.com/fchollet/keras/issues/1072
def text_to_word_sequence(text,
                          filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n',
                          lower=True, split=" "):
    if lower: text = text.lower()
    if type(text) == unicode:
        translate_table = {ord(c): ord(t) for c,t in zip(filters, split*len(filters)) }
    else:
        translate_table = maketrans(filters, split * len(filters))
    text = text.translate(translate_table)
    seq = text.split(split)
    return [i for i in seq if i]
    
keras.preprocessing.text.text_to_word_sequence = text_to_word_sequence
# end of bug

# Get the data
sentences = read_train_file(TRAIN_DATA_FILE)

tokenizer = Tokenizer(num_words=MAX_NB_WORDS)

tokenizer.fit_on_texts(sentences)

word_index = tokenizer.word_index
print('Found %s unique tokens' % len(word_index))

vocab = word_index.items()
vocab = sorted(vocab, key=itemgetter(1))

nb_words = min(MAX_NB_WORDS, len(word_index)) + 1

# %% Generate data:
x1 = list()
x2 = list()
y = list()
whole_sentences = list()

for s in sentences:
    wordlist = s.split(" ")
    sentence_embedding = prepare_embedding(wordlist, word2vec, EMBEDDING_DIM)
    x1_i, x2_i, y_i, w_s = prepare_input(sentence_embedding, wordlist)
    x1 += x1_i
    x2 += x2_i
    whole_sentences += w_s
    y += map(lambda x: np.reshape(word_index[x],(1,1)), y_i)

# %% Training data:
x1_train = x1[:-1000]
x2_train = x2[:-1000]
y_train = y[:-1000]
y_train = np.array(y_train)

# %% Test data (1K examples):
x1_test = x1[-1000:]
x2_test = x2[-1000:]
y_test = y[-1000:]
y_test = np.array(y_test)
whole_sentences = whole_sentences[-1000:]

print("finished loading data")
print("number of examples: %d" % len(x1_train))
# In[ ]:

# %% Setup input to the network and true output label. These are
# simply placeholders which we'll fill in later.
x_hierarchy_1 = tf.placeholder(tf.float32, [None, EMBEDDING_DIM], name='x_hierarchy_1') # sentence [n_words, embedding]
x_hierarchy_2 = tf.placeholder(tf.float32, [None, EMBEDDING_DIM], name='x_hierarchy_2') # first n words of sentence [n_words, embedding]
y_index = tf.placeholder(tf.int64, shape=[1, 1]) # index of next word 

# %% Since x is currently [batch, height*width], we need to reshape to a
# 4-D tensor to use it in a convolutional graph (because of tensorflow).  If one component of
# `shape` is the special value -1, the size of that dimension is
# computed so that the total size remains constant.  
# input_shape = [batch, height, width, channels]
x_hierarchy_1_tensor = tf.reshape(x_hierarchy_1, [1, 1, -1, 1])
x_hierarchy_2_tensor = tf.reshape(x_hierarchy_2, [1, -1, EMBEDDING_DIM])

# %% First convolutional layer
# Weight matrix is [height x width x input_channels x output_channels]
# W_conv1.shape = [1, 3*300, 1, 300]
filter_size = 3
n_filters_1 = EMBEDDING_DIM
W_conv1 = weight_variable([1, filter_size*EMBEDDING_DIM, 1, n_filters_1])

# %% Bias is [output_channels]
# one for each channel
b_conv1 = bias_variable([n_filters_1])

# %% Now we can build a graph which does the first layer of convolution:
# we define our stride as [batch, stride_over_height, stride_over_width, channels]
# Stride/slide one word (300) at time
# output_size = (W−F+2P)/S + 1
# Where: W = original_size, F = filter_size, P = padding, S - stride, 
# h_conv1.shape = [1, 1, ?, 300]
h_conv1 = tf.nn.tanh(tf.nn.conv2d(input=x_hierarchy_1_tensor,
                 filter=W_conv1,
                 strides=[1, 1, EMBEDDING_DIM, 1],
                 padding='VALID') + b_conv1)

# %% Max pooling on axis 2
sentence_representation = tf.reduce_max(h_conv1, axis=2, name='sentence_representation')

# %% 2nd Hierarchy
sentence_and_n_words = tf.concat([x_hierarchy_2_tensor, sentence_representation], 1)

average = tf.reduce_mean(sentence_and_n_words, axis=1)
average = tf.reshape(average, [1, -1], name='average')

# NCE: Noise Contrastive Estimation
# Construct the variables for the NCE loss                             
nce_weights = tf.Variable(                                             
  tf.truncated_normal([nb_words, EMBEDDING_DIM],             
                      stddev=1.0 / math.sqrt(EMBEDDING_DIM)))       
nce_biases = bias_variable([nb_words])

num_sampled = 25 # Number of negative examples to sample. 

# Compute the average NCE loss for the batch.
# tf.nce_loss automatically draws a new sample of the negative labels each
# time we evaluate the loss.
loss = tf.reduce_mean(
  tf.nn.nce_loss(weights=nce_weights,
                 biases=nce_biases,
                 labels=y_index,
                 inputs=average,
                 num_sampled=num_sampled,
                 num_classes=nb_words))

# average.shape: [1, 300]
# nce_weights.transpose.shape: [300, nb_words]
# output.shape: [1, nb_words]
output = tf.add(tf.matmul(average, tf.transpose(nce_weights)), nce_biases, name='output')
output_index = tf.argmax(output, 1, name='output_index')
# # y_one_hot.shape: [1, nb_words]
# y_one_hot = tf.one_hot(y_index[0], nb_words)
# loss = tf.nn.softmax_cross_entropy_with_logits(logits=output, labels=y_one_hot)

# optimizer = tf.train.AdamOptimizer(0.01).minimize(loss)
optimizer = tf.train.GradientDescentOptimizer(0.001).minimize(loss)


global_step = tf.Variable(0, name='global_step', trainable=False)

# Call this after declaring all tf.Variables.
saver = tf.train.Saver()

# %% Create a new session to actually perform the computations
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    ckpt = tf.train.get_checkpoint_state(MODEL_PATH)
    if ckpt and ckpt.model_checkpoint_path:
        print(ckpt.model_checkpoint_path)
        saver.restore(sess, ckpt.model_checkpoint_path) # restore all variables

    start = global_step.eval() # get last global_step
    print("Start from:", start)

    # %% Train (TODO: help needed to process batches):
    # resume from last global_step
    for epoch_i in range(start, 200000):
       
        # shuffle training examples
        random.seed(epoch_i)
        indexes = list(range(len(x1_train)))
        random.shuffle(indexes)
        
        time = strftime("%H:%M:%S", gmtime())
        print(time, "epoch: ", epoch_i)
        
        # Train
        hits = 0
        total_loss = 0
        for i in range(len(x1_train)):
            sess.run(optimizer, feed_dict={x_hierarchy_1: x1_train[indexes[i]], 
                                           x_hierarchy_2: x2_train[indexes[i]], 
                                           y_index: y_train[indexes[i]]})
        #     _, output_idx, step_loss = sess.run([optimizer, output_index, loss], 
        #         feed_dict={x_hierarchy_1: x1_train[indexes[i]], 
        #                    x_hierarchy_2: x2_train[indexes[i]], 
        #                    y_index: y_train[indexes[i]]})
        #     hits += 1 if output_idx == y_train[indexes[i]][0] else 0
        #     total_loss += step_loss
        # print('TRAIN: hits %d, acc %g, mean_loss %g' % (hits, hits/(i+1), total_loss/(i+1)))


        # Test
        # hits = 0
        # total_loss = 0
        # for i in range(len(x1_test)):
        #     output_idx, step_loss = sess.run([output_index, loss], feed_dict={x_hierarchy_1: x1_test[i], 
        #                                                                       x_hierarchy_2: x2_test[i], 
        #                                                                       y_index: y_test[i]})

        #     hits += 1 if output_idx == y_test[i][0] else 0
        #     total_loss += step_loss
        #     # if (i % 100 == 0 or i == 999):
        #         # print('step %d, hits %d, acc %g, loss %g, mean_loss %g' % (i, hits, hits/(i+1), step_loss, total_loss/(i+1)))
        #         # print(vocab[y_test[i][0][0]-1], vocab[output_idx[0]-1], whole_sentences[i]) # correct_label, predicted_label, sentence 
        # print('TEST: hits %d, acc %g, mean_loss %g' % (hits, hits/(i+1), total_loss/(i+1)))

        global_step.assign(epoch_i+1).eval() # set and update(eval) global_step with index, epoch_i

        # Save model weights
        saver.save(sess, MODEL_PATH + "/model.ckpt", global_step=global_step-1)

# In[ ]:
