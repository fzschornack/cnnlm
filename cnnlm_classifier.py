
# coding: utf-8

# In[2]:

# tensorflow
import tensorflow as tf

# word2vec
from gensim.models import KeyedVectors

# utils
from utils import prepare_embedding, text_to_wordlist


# In[ ]:

# Parameters
EMBEDDING_DIM = 300
EMBEDDING_FILE = 'GoogleNews-vectors-negative300.bin'
MODEL_DIR = 'model/'
META_GRAPH_PATH = 'model/model-1.meta'

# load pre-trained word2vec model (take a while...)
word2vec = KeyedVectors.load_word2vec_format(EMBEDDING_FILE,         binary=True)


# In[2]:

with tf.Session() as sess:
    # load meta graph and restore weights
    saver = tf.train.import_meta_graph(META_GRAPH_PATH)
    saver.restore(sess, tf.train.latest_checkpoint(MODEL_DIR))
    sess.run(tf.global_variables_initializer())
    
    graph = tf.get_default_graph()
    x_hierarchy_1 = graph.get_tensor_by_name("x_hierarchy_1:0")
    x_hierarchy_2 = graph.get_tensor_by_name("x_hierarchy_2:0")
    sentence_representation = graph.get_tensor_by_name("sentence_represention:0")
    average = graph.get_tensor_by_name("average:0")
    y_pred = average
    
    # Test if it works (predict)
    test = "What is the easiest way to become a billionaire($)?"
    x1_test = prepare_embedding(text_to_wordlist(test, remove_stopwords=False), word2vec, EMBEDDING_DIM)
    x2_test = x1_test[:2] # [throw, the]
    wordlist = text_to_wordlist(test, remove_stopwords=False)
    print(wordlist[:2], wordlist[3])
    # vector_pred should be [ball]
    vector_pred = sess.run(y_pred, feed_dict={x_hierarchy_1: x1_test, 
                                           x_hierarchy_2: x2_test})

    # Word2vec needs shape (300,) as input
    vector_pred = vector_pred.reshape(300)
    most_similar_word = word2vec.most_similar(positive=[vector_pred], topn=10)
    print(most_similar_word)


# In[ ]:



