
# coding: utf-8

# In[2]:

# tensorflow
import tensorflow as tf

# word2vec
from gensim.models import KeyedVectors

# utils
from utils import prepare_embedding, read_train_file
from scipy import spatial

import numpy as np

# In[ ]:

# Parameters
TEST_DATA_FILE = '../quora_test.csv'
EMBEDDING_DIM = 300
EMBEDDING_FILE = 'GoogleNews-vectors-negative300.bin'
MODEL_DIR = 'model/'
META_GRAPH_PATH = 'model/model.ckpt-1559.meta'

# load pre-trained word2vec model (take a while...)
# word2vec = KeyedVectors.load_word2vec_format(EMBEDDING_FILE,         binary=True)


# In[2]:

with tf.Session() as sess:
    # load meta graph and restore weights
    saver = tf.train.import_meta_graph(META_GRAPH_PATH)
    saver.restore(sess, tf.train.latest_checkpoint(MODEL_DIR))
    sess.run(tf.global_variables_initializer())
    
    graph = tf.get_default_graph()
    x_hierarchy_1 = graph.get_tensor_by_name("x_hierarchy_1:0")
    x_hierarchy_2 = graph.get_tensor_by_name("x_hierarchy_2:0")
    sentence_representation = graph.get_tensor_by_name("sentence_representation:0")
    # average = graph.get_tensor_by_name("average:0")
    # y_pred = average
    
    # names = [n.name for n in graph.as_graph_def().node]

    # print(names)

    # w = graph.get_tensor_by_name("Variable:0")
    # b = graph.get_tensor_by_name("Variable_1:0")

    # print(w.shape, b.shape) 
    # print(sess.run(w))
    # print(sess.run(tf.reduce_max(tf.reshape(w,[900*300]), 0)))

    text_test_1, text_test_2, y_label = read_train_file(TEST_DATA_FILE)
    
    # Test if it works (predict)
    for i in range(len(text_test_1)):

        x1_test = prepare_embedding(text_test_1[i].rstrip().split(" "), word2vec, EMBEDDING_DIM)
        x2_test = prepare_embedding(text_test_2[i].rstrip().split(" "), word2vec, EMBEDDING_DIM)
        # vector_pred should be [ball]
        sentence_representation_1 = sess.run(sentence_representation, feed_dict={x_hierarchy_1: x1_test})
        sentence_representation_2 = sess.run(sentence_representation, feed_dict={x_hierarchy_1: x2_test})

        sentence_representation_1 = np.reshape(sentence_representation_1, (-1))
        sentence_representation_2 = np.reshape(sentence_representation_2, (-1))

        cos_dist = spatial.distance.cosine(sentence_representation_1, sentence_representation_2)

        print(cos_dist, y_label[i])

    Word2vec needs shape (300,) as input
    vector_pred = vector_pred.reshape(300)
    most_similar_word = word2vec.most_similar(positive=[vector_pred], topn=10)
    print(most_similar_word)


# In[ ]:



